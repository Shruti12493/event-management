({
    doInit: function(component, event, helper) {

        debugger;
       helper.doInitHelper(component,event,helper);
    },
    saveLayout: function(component) {

        debugger;
        var tablesList = component.get("v.wrapperList.tables");
        var updatedWrapperList = component.get("v.tempList");
        var action = component.get("c.saveVenueLayout");
        for (var i = 0, len = updatedWrapperList.length; i < len; i++) {
            delete updatedWrapperList[i].rowNo;
            delete updatedWrapperList[i].colNo;
        }
        component.set("v.wrapperList.tables", updatedWrapperList);
        console.log("updatedWrapperList" + updatedWrapperList);
        action.setParams({ "wrapper": component.get("v.wrapperList") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'ERROR') {

            } else if (state === 'SUCCESS') {
                debugger;
            }
        });
        $A.enqueueAction(action);
    },

    

    backToVenueDetails: function(component, event, helper) {
        debugger;
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef: "c:venuelayout",
            componentAttributes: {
                eventvenueId: component.get("v.eventvenueId")
            }
        });
        evt.fire();
        event.preventDefault();
    },
    handleApplicationEvent: function(component, event, helper) {

        debugger;
        var message = event.getParam("message");
        alert('message NumberOfSeats' + message.NumberOfSeats);
        alert('message tableName' + message.tableName);
        alert('message layoutType' + message.layoutType);
        var tableList = component.get("v.wrapperList.tables");
        for (var i = 0; i < tableList.length; i++) {
            if (message.tableName === tableList[i].tableName) {
                tableList[i].NumberOfSeats = 9;
            }
        }
        component.set("v.wrapperList.tables", tableList);
        component.set("v.seatsPerTableAfterEdit", message.NumberOfSeats);
        component.set("v.tableNameAfterEdit", message.tableName);
        component.set("v.layoutTypeAfterEdit", message.layoutType);
    },
    /*drop : function(event) {
        debugger;
        var listitem = event.dataTransfer.getData("divid");
        
        console.log("Inside drop =====> "+listitem);    
        alert("element dropped here    "+listitem);
    },
    
    allowDrop: function(event){
        event.preventDefault();
    }*/
    onDragOver: function(component, event) {
        debugger;
        event.preventDefault();
    },

    drop: function(component, event, helper) {
        debugger;
        event.stopPropagation();
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy';
        var files = event.dataTransfer.files;
        if (files.length > 1) {
            return alert("You can only upload one profile picture");
        }

    },
    
    handleApplicationEventRender : function(component, event, helper) {
         var wrapper = event.getParam("wrapper");
        var reRenderFlag = event.getParam("reRenderFlag");

        // set the handler attributes based on event data
        component.set("v.wrapperRender", wrapper);
         component.set("v.reRenderFlag", reRenderFlag);
         helper.doInitHelper(component,event,helper);
    }
    
   
})