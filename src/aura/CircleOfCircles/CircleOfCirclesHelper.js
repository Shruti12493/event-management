({
	drawDefaultVenueLayout : function(component) {
        
        debugger;
        var containerdiv = document.getElementById("containerdiv");
        var tableId = component.get("v.tableId");
		var parentdiv = document.createElement('div');
        var div = 360/10;
        parentdiv.className = 'parentCircle';
        parentdiv.id = 'table';
        parentdiv.style.position = 'absolute';
        containerdiv.appendChild(parentdiv);
        var height = document.getElementById('table').offsetHeight;
        var width = document.getElementById('table').offsetWidth;
        var numberOfSeats = 100;
        var radius = 180;
        var offsetToParentCenter = parseInt(parentdiv.offsetWidth / 2);  //assumes parent is square
        var offsetToChildCenter = 20;
        var totalOffset = offsetToParentCenter - offsetToChildCenter;
        for (var j = 1; j <= 60; ++j){
            
            if(j <= "10"){
                div = 360/10;
                radius = 90;
            }else if( j > "10" && j <= "30"){
                div = 360/20;
                radius = 135;
               // document.getElementById('table').setAttribute("style","width:180px;height:180px");
            }else if( j > "30" && j <= "70"){
                div = 360/30;
               // document.getElementById('table').setAttribute("style","width:270px;height:270px");
                radius = 180; 
            }
            var childdiv = document.createElement('div');
            childdiv.className = 'div2';
            childdiv.style.position = 'absolute';
            var y = Math.sin((div * j) * (Math.PI / 180)) * radius;
            var x = Math.cos((div * j) * (Math.PI / 180)) * radius;
            childdiv.style.top = (y + totalOffset).toString() + "px";
            childdiv.style.left = (x + totalOffset).toString() + "px";
            parentdiv.appendChild(childdiv);
            
        }
    }
})