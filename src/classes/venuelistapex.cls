public class venuelistapex {
    @AuraEnabled
    public static List<Venue__c> aLLVenue(){
        return [Select Id,Name,City__c,Country__c,Location__c,Seating_Capacity__c,Type__c,Active__c from Venue__c];
    }
     @AuraEnabled
    public static List<Venue__c> findVenueByName(String searchKey) {
        String name = '%' + searchKey + '%';
        String city = '%' + searchKey + '%';
        return [SELECT Id, Name, City__c,Country__c,Seating_Capacity__c,Type__c,Active__c FROM Venue__c WHERE (Name LIKE :name OR City__c LIKE :city) AND ParentVenue__c =: null];
    }
    @AuraEnabled
    public static List<Venue__c> getsubVenueList(List<string> subvenueId) {
        List<Venue__c> resultList = [select Id,Name,ParentVenue__r.Name,City__c,Country__c,Seating_Capacity__c,Type__c,Active__c from Venue__c where ParentVenue__c IN: subvenueId];
        return resultList;
    }
     
    
    
    
    
}